import {getHeroeById} from './bases/08-imp-exp'

const getHeroeByIdAsync =  (id) => {


    return new Promise( (resolve, reject) => {

        //simulamos que la llamada a un API que tarda 1 segundo en responder 
        setTimeout(  () => {

            
            const heroe = getHeroeById(id)

            
            if (heroe){
                //el heroe existe
                resolve(heroe)
            }else{
                //el heroe no existe
                reject('El heroe no existe')
            }


            resolve(heroe)

        }, 1000);

    });

}

getHeroeByIdAsync(5)
    .then(h => {
        console.log(`El heroe es ${h.name}`)
    })
    .catch(console.log )