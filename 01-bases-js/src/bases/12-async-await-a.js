const miPromesa = () => {
    return new Promise((resolve, reject) => {

        setTimeout( () => {

            //resolve('tenemos un valor en la promesa') 
            reject('reject en mi promesa')

        },1000);



    }) 
}

//async hace que una funcion normal retorne una promesa
const medirTiempoAsync = async() => {


    try {
        
        console.log('inicio')

        //espera a la resolucion de la promesa
        const respuesta = await miPromesa()
        console.log(respuesta)
        
        
        console.log('fin')
    
        return 'fin de medirIiempoAsync'
        //throw 'Error en medir tiempo async'

    } catch (error) {
        return 'catch en medirIiempoAsync'
        //throw 'Error en medirIiempoAsync' //esto dispara el catch de la promesa
    }
}


medirTiempoAsync()
     .then(valor => console.log('then exitoso: ' + valor))   //valor esperado
     .catch(err => console.log('catch de la promesa: ' + err)) //cuando hacemos un throw