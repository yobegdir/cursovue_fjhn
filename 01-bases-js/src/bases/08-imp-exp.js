import {owners} from '../data/heroes'

//importacion por defecto
import superheroes from '../data/heroes'

//console.log(owners) 


const [dc, marvel] = owners


//console.log(dc) 
//console.log(marvel) 


//console.log(superheroes)

//usar el metodo find de los arrays
export const getHeroeById = (id) =>  superheroes.find((elem) => elem.id === id)
    


//console.log('llamamos a getHeroeById(2)') 
//console.log(getHeroeById(2)) //Spiderman


//usar el metodo filter de los arrays
//devuelve un array con los heroes del owner pasado por parametro
export const getHeroesByOwner = (owner) => superheroes.filter(heroe => heroe.owner === owner)

//console.log(getHeroesByOwner('DC')) //Batman, Superman, Flash
//console.log(getHeroesByOwner('Marvel')) 

// si lo llamamos desde un index.js al nivel de la carpeta 'bases' importando
//sería así

/*

import {getHeroeById, getHeroesByOwner} from './bases/08-imp-exp'


console.log('llamamos a getHeroeById(2)') 
console.log(getHeroeById(2)) //Spiderman

console.log('llamamos a getHeroesByOwner(DC)') 
console.log(getHeroesByOwner('DC')) //Batman, Superman, Flash

*/

