

//funcion normal
function saludar(nombre){
    return `hola ${nombre}`
}

//funcion normal coon constante (evita reasignacion)
const saludar2 = function(nombre){
    return `hola ${nombre}`
}

//funcion flecha con return explicito
const saludar3  = (nombre) => {
    return `hola ${nombre}`
}

//funcion flecha con return implicito
const saludar4  = (nombre) => `hola ${nombre}`

//funcion flecha con valor por defecto en la entrada
const saludar5  = (nombre = 'Peter' ) => `hola ${nombre}`

//return explicito en objeto
const getUser = () => {
    return {
        uid: 'ABC123',
        username : 'Tony001'
    }
}

//return implicito en objeto
const getUser2 = () => ({uid: 'ABC123',username : 'Tony001'})

//----------------------------------------
const nombre = 'tony'

console.log(saludar(nombre))
console.log(saludar2(nombre))
console.log(saludar3(nombre))
console.log(saludar4(nombre))
console.log(saludar5())
console.log(getUser())
console.log(getUser2())


console.log('Ejercicio heroes existe')

const heroes = [
    {
        id : 1,
        name : 'batman'
    },
    {
        id : 2,
        name : 'superman'
    }
]


function checkAvailability(arr, valor){
    return arr.some((arrValor) => arrValor.id === valor);
}

const existe1 = checkAvailability(heroes, 1);
const existe2 = checkAvailability(heroes, 3);


console.log(existe1)
console.log(existe2)


console.log('Ejercicio heroes existe con funciones de flecha')

const existe3 = heroes.some((arrValor) => arrValor.id === 1)
const existe4 = heroes.some((arrValor) => arrValor.id === 3)

console.log(existe3)
console.log(existe4)