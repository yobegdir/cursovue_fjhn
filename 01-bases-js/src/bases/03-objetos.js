const persona = {
    nombre : 'Tony',
    apellido : 'Stark',
    edad : 45,
    direccion : {
        ciudad : 'New york',
        zip : 54784,
        lat: 14.3232,
        lng: 34.1589
    }
}

const persona2 = {...persona}

persona2.nombre = 'Peter'

console.log(persona2)
console.log(persona)