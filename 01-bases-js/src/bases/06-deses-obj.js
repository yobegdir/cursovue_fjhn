console.log('desestructuracion 1')
const person = {
    name: 'tony',
    age: 45,
    codename :'Iron man'
}

//exrtraemos las propiedades de person de una vez
//ojo q las variables de destino se tienen q llamar = 
// q las propiedades del obj, xq si no  devuelve 
// undefined
const {name, age, codename, power = 'no tiene poder'} = person

//  console.log(person.name)
//  console.log(person.age)
//  console.log(person.codename)

console.log(name)
console.log(age)
console.log(codename)
console.log(power)

console.log('desestructuracion 2')
const person2 = {
    name2: 'clark',
    age2: 30,
    codename2 :'superman',
    power2 : 'rayos laser'
}

const {name2, age2, codename2, power2 = 'no tiene poder'} = person2

console.log(name2)
console.log(age2)
console.log(codename2)
console.log(power2)

console.log ('llamamos a createHero')
const createHero = ({name2, age2, codename2, power2 = 'no tiene poder'}) => {

    //const {name2, age2, codename2, power2 = 'no tiene poder'} = persona

    return {
        id : 654984981, 
        name: name2,
        age: age2,
        codename: codename2,
        power: power2,
    }
}

console.log(createHero(person2))

console.log ('llamamos a createHero2')
// desestructura y devuelve de manera implícita el obj js con el uso de ()
//cambiamos el nombre de las propiedades recibidas en la llamada
const createHero2 = ({name2:nombre, age2:edad, codename2:nombreclave, power2:poder = 'no tiene poder'}) => ({ id : 654984981, nombre, edad, nombreclave, poder})

    
console.log(createHero2(person2))
    
