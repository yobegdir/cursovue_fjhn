import { createRouter, createWebHashHistory } from "vue-router"
import isAuthenticatedGuard from './auth-guard'


/** 
* *carga perezosa
*/
const routes = [
    {
        path: '/',
        redirect: '/pokemon'
    },  
    {
        path: '/pokemon',
        name: 'pokemon',
        component: () => import (/* webpackChunkName:"PokemonLayout" */ '@/modules/pokemon/layouts/PokemonLayout'),
        /** 
        * * en las rutas hijas el path no suele comenzar por '/'
        */
        children: [
            { 
                path: 'home',  
                name: 'pokemon-home',
                component: () => import(/* webpackChunkName:"ListPage" */ '@/modules/pokemon/pages/ListPage')
            },
            { 
                path: 'about', 
                name: 'pokemon-about', 
                component: ()  => import (/* webpackChunkName:"AboutPage" */ '@/modules/pokemon/pages/AboutPage') 
            },
            { 
                path: 'pokemonid/:id', 
                name: 'pokemon-id',
                component: () => import (/* webpackChunkName:"PokemonPage" */ '@/modules/pokemon/pages/PokemonPage'),
                props: (route) => {
                    const id = Number(route.params.id)
                    return isNaN(id) ? { id: 1 } : { id }
                }
            },
            {
                path: '', // * * path por defecto  si entramos al path del padre
                redirect: {name: 'pokemon-about'}
            }
        ]
    },
    {
        path: '/dbz',
        name: 'dbz',
        beforeEnter: [ isAuthenticatedGuard ],
        component: () => import (/* webpackChunkName:"DragonBallLayout" */ '@/modules/dbz/layouts/DragonBallLayout'),
        /** 
        * * en las rutas hijas el path no suele comenzar por '/'
        */
        children: [
            { 
                path: 'characters',  
                name: 'dbz-characters',
                component: () => import(/* webpackChunkName:"Characters" */ '@/modules/dbz/pages/Characters')
            },
            { 
                path: 'about', 
                name: 'dbz-about', 
                component: ()  => import (/* webpackChunkName:"About" */ '@/modules/dbz/pages/About') 
            },
            {
                path: '', // * * path por defecto  si entramos al path del padre
                redirect: {name: 'dbz-characters'}
            }
        ]
    },
    { 
        path: '/:pathMatch(.*)*', 
        component: () => import (/* webpackChunkName:"NoPageFound" */ '@/modules/shared/pages/NoPageFound') 
    }
]


const router = createRouter({
    history: createWebHashHistory(),
    routes, // * * short for `routes: routes`
})

// ** Guard Global - Sincrono
// router.beforeEach( (to, from , next) => {
//     const random = Math.random() * 100


//     if (random > 50){
//         console.log('autenticado')
//         next() //**deja acceder a la pantalla de destino
//     }else{
//         console.log(random, 'bloqueado por el beforeEach Guard')
//         next({name : 'pokemon-home'})
//     }
// })

// const canAccess = () => {
//     return new Promise(resolve => {

//         const random = Math.random() * 100
//         if (random > 50){
//             console.log('autenticado - canAccess')
//             resolve(true) //**deja acceder a la pantalla de destino
//         }else{
//             console.log(random, 'bloqueado por el beforeEach Guard - canAccess')
//             resolve(false)
//         }

//     })
// }

// ** Guard Global - Asincrono
//** argumentos posicionales
// router.beforeEach( async(to, from , next) => {

//     const authorized = await canAccess()

//     authorized ? next() : next({name : 'pokemon-home'})

// })

export default router