import { shallowMount } from '@vue/test-utils'
import Indecision from '@/components/Indecision'


describe('Indecision component', () => {

    let wrapper
    let clgSpy

    //mock de window.fetch de los navegadores web
    global.fetch = jest.fn( () => Promise.resolve({

        json : () => Promise.resolve({
            answer: 'yes',
            forced: false,
            image: 'https://yesno.wtf/assets/yes/2.gif'
        })

    }))

    beforeEach(() => {
        wrapper = shallowMount(Indecision);

        clgSpy = jest.spyOn(console, 'log')

        jest.clearAllMocks()

    })

    test('debe de hacer match con el snapshot', () => {

        expect( wrapper.html() ).toMatchSnapshot()

    })


    test('escribir en el input no debe de disparar nada (console.log)', async() => {

        //preguntamos al metodo getAnswer de la instancia de Vue
        const getAnswerSpy = jest.spyOn( wrapper.vm, 'getAnswer' )

        const input = wrapper.find('input')

        //esperamos a q el DOM se actualice
        await input.setValue('Hola mundo')

        //deberia haber llamado 2 veces al console.log
        expect(clgSpy).toHaveBeenCalledTimes(1)

        expect(getAnswerSpy).not.toHaveBeenCalled()


    });


    test('escribir el simbolo de "?" debe disparar el getAnswer', async() => {

        //wrapper.vm === objeto VUE de nuestro componente

        const getAnswerSpy = jest.spyOn( wrapper.vm, 'getAnswer' )

        const input = wrapper.find('input')

        await input.setValue('Hola mundo?')

        expect(getAnswerSpy).toHaveBeenCalled()
        
    });

    test('pruebas en getAnswer', async () => {
       
        await wrapper.vm.getAnswer()

        const img = wrapper.find('img')


        expect( img.exists() ).toBeTruthy()
        expect( wrapper.vm.img ).toBe('https://yesno.wtf/assets/yes/2.gif')
        expect( wrapper.vm.answer ).toBe('Si!')
    });

    test('pruebas en getAnswer - fallo en el API', async () => {

        //mockeamos un fallo del fetch mock
        fetch.mockImplementationOnce( () => Promise.reject('API is down') )

        await wrapper.vm.getAnswer()

        const img = wrapper.find('img')

        expect( img.exists() ).toBeFalsy()
        expect( wrapper.vm.answer ).toBe('No se pudo cargar del API')

    });


})