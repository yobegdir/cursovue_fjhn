import { shallowMount } from '@vue/test-utils'
import Counter from '@/components/Counter'

describe('Counter component', () => {

    let wrapper

    beforeEach(() => {
        wrapper = shallowMount(Counter);
    })



    test('debe de hacer match con el snapshot', () => {

        //const wrapper = shallowMount(Counter)

        expect( wrapper.html() ).toMatchSnapshot()

    }) 


    test('h2 deberia tener el valor por defecto de "Counter', () => {
        
        //const wrapper = shallowMount(Counter)

        expect(wrapper.find('h2').exists()).toBeTruthy()

        const h2value = wrapper.find('h2').text()

        expect(h2value).toBe('Counter')

    });

    test('el valor x defecto en el P debe ser 100', () => {

        //wrapper?
        //const wrapper = shallowMount(Counter)

        //pTags 
        //const pTags = wrapper.findAll('p')
        const value = wrapper.find('[data-test-id="counter"]').text()

        
        //expect segundo P = 100
        //expect(pTags[1].text()).toBe('100') 
        expect(value).toBe('100')
        
    });


    test('debe de incrementar y decrementar el contador ', async () => {


        //const wrapper = shallowMount(Counter)

        const [increaseBtn, decreaseBtn] = wrapper.findAll('button')

        await increaseBtn.trigger('click')
        await increaseBtn.trigger('click')
        await increaseBtn.trigger('click')
        await decreaseBtn.trigger('click')
        await decreaseBtn.trigger('click')

        const value = wrapper.find('[data-test-id="counter"]').text()

        expect(value).toBe('101')
        
    });


    test('debe de establecer el valor por defecto', () => {
       
        const {start} = wrapper.props()
        
        const value = wrapper.find('[data-test-id="counter"]').text()

        expect( Number(value) ).toBe(start)

    });


    test('debe de mostrar la prop "title" ', () => {
       
        const wrapper = shallowMount(Counter, {
            props:{
                title: 'hola mundo'
            }
        }); 

        
        console.log(wrapper.find('h2'))


        //console.log(wrapper.find('h2').text()).toBe('hola mundo');
        console.log(wrapper.find('h2').text())


        
    });


})