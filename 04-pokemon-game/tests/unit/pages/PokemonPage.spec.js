import { shallowMount, mount  } from "@vue/test-utils"
import PokemonPage from "@/pages/PokemonPage"
import { pokemons } from "../mocks/pokemons.mock";



describe('PokemonPage component', () => {
   
    let wrapper
    let mixPokemonArraySpy
    
    beforeEach(() => {
        mixPokemonArraySpy = jest.spyOn(PokemonPage.methods, 'mixPokemonArray')

        wrapper = shallowMount(PokemonPage)
    })


    test('debe de hacer match con el snapshot ', () => {
        
        //console.log(wrapper.html());

        expect(wrapper.html()).toMatchSnapshot()

    });

    test('debe de llamar a "mixPokemonArray" al montar', () => {
        
        expect(mixPokemonArraySpy).toHaveBeenCalled()

    });

    test('debe de hacer match con el snapshot cuando cargan los pokemons', () => {
    
        const wrapper = shallowMount( PokemonPage, {
            data(){
                return{
                    pokemonArr : [pokemons],
                    pokemon : pokemons[0],
                    showPokemon: false,
                    showAnswer: false,
                    message: ''
                }
            }
        })

        expect( wrapper.html()).toMatchSnapshot()
        
    });


    test('debe de mostrar los componentes de PokemonPîcture  y PokemonOptions', () => {


        const wrapper = shallowMount( PokemonPage, {
            data(){
                return{
                    pokemonArr : [pokemons],
                    pokemon : pokemons[0],
                    showPokemon: false,
                    showAnswer: false,
                    message: ''
                }
            }
        })


        const picture = wrapper.find("pokemon-picture-stub")
        const options = wrapper.find("pokemon-options-stub")

        //PokemonPîcture debe de existir 
        expect( picture.exists()).toBeTruthy()


        //PokemonOptions debe de existir
        expect( options.exists()).toBeTruthy()
        
        
        //PokemonPîcture tenga el atributo pokemonId === 5
        expect( picture.attributes("pokemonid")).toBe("5")

        expect( options.attributes("pokemons")).toBeTruthy()
    });

    test('Pruebas con checkAnswer ', async () => {

        const wrapper = shallowMount( PokemonPage, {
            data(){
                return{
                    pokemonArr : [pokemons],
                    pokemon : pokemons[0],
                    showPokemon: false,
                    showAnswer: false,
                    message: ''
                }
            }
        })

        //como message se dibuja en el DOM (renderización del componente, los watchers, el ciclo de vida , etc), 
        //hay q esperar a q el DOM virtual del shallowMount tb lo renderice, de ahí viene el await --> función asíncrona
        await wrapper.vm.checkAnswer(5)


        expect(wrapper.find('h2').exists()).toBeTruthy()
        expect(wrapper.vm.showPokemon ).toBeTruthy()
        expect(wrapper.find('h2').text()).toBe(`Correcto, ${pokemons[0].name}`)


        //console.log(wrapper.vm.messsage)

        await wrapper.vm.checkAnswer(10)
        expect(wrapper.vm.message).toBe(`Oops era ${pokemons[0].name}`)

        
    });
});