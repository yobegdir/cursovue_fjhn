//no se pone {pokemonApi} xq hacemos el export default en /api/pokemonApi.js
import pokemonApi from "../api/pokemonApi";


//creamos el array
export const getPokemons = () => {

    const pokemonArr = Array.from(Array(650))

    //no me interesa el 1er param del map
    return pokemonArr.map( ( _ , index ) => index + 1 )

}


//mezclamos 
const getPokemonOptions = async () => {

    const mixedPokemons = getPokemons().sort( () => Math.random() - 0.5 )


    //console.log(mixedPokemons)

    //mandamos los primeros 4 elementos
    const pokemons =  await getPokemonNames(mixedPokemons.splice(0,4))

    //console.table(pokemons);
    return pokemons

}


// la entrada de la funcion es una desestructuración del array que le pasamos
export const getPokemonNames =  async( [a,b,c,d] = [] ) => {

    //const resp = await pokemonApi.get(`/1`)
    //console.log(resp.data.name, resp.data.id);


    const promiseArr = [
        pokemonApi.get(`/${ a }`),
        pokemonApi.get(`/${ b }`),
        pokemonApi.get(`/${ c }`),
        pokemonApi.get(`/${ d }`)
    ]

    //dispara a la vez todas las promesas recibidas en el array y devuelve los resultados  en un array
    const [ p1, p2, p3, p4 ] = await Promise.all( promiseArr )

    
    return  [
        {name: p1.data.name, id : p1.data.id}, 
        {name: p2.data.name, id : p2.data.id}, 
        {name: p3.data.name, id : p3.data.id}, 
        {name: p4.data.name, id : p4.data.id}
    ]


    //console.log(a,b,c,d)

}

export default getPokemonOptions