/* eslint-disable */

import createVuexStore from "../../../mock-data/mock-store"
import axios from "axios"


describe('Vuex: Pruebas en el auth-module', () => {
    
    test('estado inicial ', () => {
        
        const store = createVuexStore({

            status: 'authenticating', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })

        const { status, user, idToken, refreshToken } = store.state.auth

        expect( status ).toBe( 'authenticating' )
        expect( user ).toBe( null )
        expect( idToken ).toBe( null )
        expect( refreshToken ).toBe( null )

    })

    test('mutations: loginUserMutation ', () => {

        const store = createVuexStore({

            status: 'authenticating', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })

        const payload = {
            user : { name : 'yomismo', email : 'prueba@prueba.com'},
            idToken : 'ABC-123',
            refreshToken : 'XYZ-456'
        }

        store.commit('auth/loginUserMutation', payload)

        const { status, user, idToken, refreshToken } = store.state.auth

        expect( status ).toBe( 'authenticated' )
        expect( user ).toEqual( { name : 'yomismo', email : 'prueba@prueba.com'} )
        expect( idToken ).toBe( 'ABC-123' )
        expect( refreshToken ).toBe( 'XYZ-456' )

    })

    test('mutations: logoutMutation ', () => {


        localStorage.setItem('idToken', 'ABC-123')
        localStorage.setItem('refreshToken', 'XYZ-456')


        const store = createVuexStore({

            status: 'authenticated', // 'authenticated', 'not-authenticated', authenticating
            user: { name : 'yomismo', email : 'prueba@prueba.com'}, 
            idToken: 'ABC-123',
            refreshToken: 'XYZ-456'

        })


        store.commit('auth/logoutMutation')

        const { status, user, idToken, refreshToken } = store.state.auth

        expect( status ).toBe( 'not-authenticated' )
        expect( user ).toBeFalsy()
        expect( idToken ).toBeFalsy()
        expect( refreshToken ).toBeFalsy()

        expect( localStorage.getItem('idToken')).toBeFalsy()
        expect( localStorage.getItem('refreshToken')).toBeFalsy()

    })


    test('getters: currentState y username ', () => {

        const store = createVuexStore({

            status: 'authenticated', // 'authenticated', 'not-authenticated', authenticating
            user: { name : 'yomismo', email : 'prueba@prueba.com'}, 
            idToken: 'ABC-123',
            refreshToken: 'XYZ-456'

        })
        
        

        expect( store.getters['auth/currentState'] ).toBe( 'authenticated' )
        expect( store.getters['auth/username'] ).toBe( 'yomismo' )

        
    })

    test('actions: createUser - Error usuario ya existe ', async() => {

        const store = createVuexStore({

            status: 'not-authenticated', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })
        
        const newUser = { name : 'test user', email : 'test@test.com', password: '123456' }

        
        const resp = await store.dispatch('auth/createUserAction', newUser)
        
        expect( resp ).toEqual({ ok: false, message: 'EMAIL_EXISTS' })

        const { status, user, idToken, refreshToken } = store.state.auth

        expect( status ).toBe( 'not-authenticated' )
        expect( user ).toBeFalsy()
        expect( idToken ).toBeFalsy()
        expect( refreshToken ).toBeFalsy()

        
    })

    test('actions:  creteUser signUser - Crea el usuario', async () => {
      
        const store = createVuexStore({

            status: 'not-authenticated', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })
        
        const newUser = { name : 'test user', email : 'test2@test2.com', password: '123456' }

        //signIn del usuario
        await store.dispatch('auth/signInUserAction', newUser)
        const { idToken } = store.state.auth

        //borrar el usuario
        const deleteResponse = await axios.post(`https://identitytoolkit.googleapis.com/v1/accounts:delete?key=AIzaSyDQwfTsFbByVr9xb6xZl0YCmjlWhDMCaw4`, { idToken })

        
        //crear el usuario
        const resp = await store.dispatch('auth/createUserAction', newUser)

        expect(resp).toEqual({ ok: true })

        const { status, user, idToken: token, refreshToken } = store.state.auth

        expect( status ).toBe( 'authenticated' )
        expect( user ).toMatchObject( { name : 'test user', email : 'test2@test2.com' } )
        expect( typeof token ).toBe( 'string' )
        expect( typeof refreshToken ).toBe( 'string' )

    })


    test('actions: checkAuthentication - validada ', async () => {
        
        const store = createVuexStore({

            status: 'authenticating', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })

        //signIn 
        await store.dispatch('auth/signInUserAction', { name : 'User Test', email : 'test@test.com', password: '123456' })
        const { idToken }  = store.state.auth

        store.commit('auth/logoutMutation')


        //el de antes del commit a 'logoutMutation'
        localStorage.setItem('idToken', idToken)


        const checkResp = await store.dispatch('auth/checkAuthenticationAction')
        const { status, user, idToken: token, refreshToken } = store.state.auth

        expect(checkResp).toEqual({ ok: true })     

        expect( status ).toBe( 'authenticated' )
        expect( user ).toMatchObject( { name : 'User Test', email : 'test@test.com' } )
        expect( typeof token ).toBe( 'string' )

    })


    test('actions: checkAuthentication - no validada ', async () => {

        const store = createVuexStore({

            status: 'authenticating', // 'authenticated', 'not-authenticated', authenticating
            user: null, 
            idToken: null,
            refreshToken: null

        })

        localStorage.removeItem('idToken')

        const checkResp1 = await store.dispatch('auth/checkAuthenticationAction')

        expect(checkResp1).toEqual({ ok: false, message: 'No hay token en la petición' })
        expect( store.state.auth.user ).toBeFalsy
        expect( store.state.auth.status ).not.toBe('authenticated')
        expect( store.state.auth.idToken ).toBeFalsy


        localStorage.setItem('idToken','ABC-123')

        const checkResp2 = await store.dispatch('auth/checkAuthenticationAction')

        expect(checkResp2).toEqual({ ok: false, message: 'INVALID_ID_TOKEN' })
        expect( store.state.auth.status ).not.toBe('authenticated')
    })

})
