/* eslint-disable */

import useAuth from "@/modules/auth/composables/useAuth"

const mockStore = {
    dispatch : jest.fn(),
    commit : jest.fn(),
    getters : {
        'auth/currentState' : 'authenticated',
        'auth/username' : 'pepito'
    }
}

//mockeamos librerias 
jest.mock('vuex', () => ({
    useStore: () => mockStore
}))



describe('Pruebas con el composable useAuth', () => {

    beforeEach( () => {

        jest.clearAllMocks()

    }) 
  
    test('createUser ok ', async () => {
      
        const { createUserAction } = useAuth()

        const newUser = { name : 'User Test', email : 'test@test.com', password: '123456' }

        //simulamos q al llamar a 'mockStore.dispatch', retorne '{ok: true}'
        mockStore.dispatch.mockReturnValue( {ok: true} )

        const resp = await createUserAction( newUser )

        expect( mockStore.dispatch ).toHaveBeenCalledWith("auth/createUserAction", newUser)
        
        expect( resp ).toEqual( {ok: true} )
    })

    test('createUser ko, porque el usuario/email ya existe ', async() => {

        const { createUserAction } = useAuth()

        const newUser = { name : 'User Test', email : 'test@test.com', password: '123456' }

        //simulamos q al llamar a 'mockStore.dispatch', retorne '{ok: false, message: 'EMAIL_EXISTS'}'
        mockStore.dispatch.mockReturnValue( {ok: false, message: 'EMAIL_EXISTS'} )

        const resp = await createUserAction( newUser )

        expect( mockStore.dispatch ).toHaveBeenCalledWith("auth/createUserAction", newUser)


        expect( resp ).toEqual( {ok: false, message: 'EMAIL_EXISTS'} )

    })

    test('loginUser ok ', async() => {
       
        const { loginUserAction } = useAuth()

        const loginForm = { email : 'test@test.com', password: '123456' }

        //simulamos q al llamar a 'mockStore.dispatch', retorne '{ok: true}'
        mockStore.dispatch.mockReturnValue( {ok: true} )

        const resp = await loginUserAction( loginForm )

        expect( mockStore.dispatch ).toHaveBeenCalledWith("auth/signInUserAction", loginForm)
        
        expect( resp ).toEqual( {ok: true} )

    })

    test('loginUser ko ', async() => {
       
        const { loginUserAction } = useAuth()

        const loginForm = { email : 'test@test.com', password: '123456' }

        //simulamos q al llamar a 'mockStore.dispatch', retorne '{ok: true}'
        mockStore.dispatch.mockReturnValue( {ok: false, message: 'EMAIL/PASSWORD do not exist'} )

        const resp = await loginUserAction( loginForm )

        expect( mockStore.dispatch ).toHaveBeenCalledWith("auth/signInUserAction", loginForm)
        
        expect( resp ).toEqual( {ok: false, message: 'EMAIL/PASSWORD do not exist'} )

    })

    test('checkStatus ok ', async() => {
       
        const { checkStatusAction } = useAuth()

        //simulamos q al llamar a 'mockStore.dispatch', retorne '{ok: true}'
        mockStore.dispatch.mockReturnValue( {ok: true} )

        const resp = await checkStatusAction()

        expect( mockStore.dispatch ).toHaveBeenCalledWith("auth/checkAuthenticationAction")
        
        expect( resp ).toEqual( {ok: true} )

    })

    test('logout ', () => {

        const { logout } = useAuth()

        logout()

        expect( mockStore.commit ).toHaveBeenCalledWith("auth/logoutMutation")
        expect( mockStore.commit ).toHaveBeenCalledWith("journal/clearEntriesMutation")

    })

    test('computed authState, username ', () => {
        
        const { authStatus, username } = useAuth()

        expect(authStatus.value).toBe('authenticated')
        expect(username.value).toBe('pepito')

    })
})
