/* eslint-disable */

import { shallowMount, config  } from "@vue/test-utils"
import LoginView from '@/modules/auth/views/LoginView.vue'
import { VueRouterMock,createRouterMock,injectRouterMock } from 'vue-router-mock'
import createVuexStore from '../../../mock-data/mock-store' 
import Swal from 'sweetalert2'


jest.mock('sweetalert2', () => ({ 
    fire: jest.fn(),
    showLoading: jest.fn(),
    close: jest.fn()
}))

// create one router per test file
const router = createRouterMock()

beforeEach(() => {
    router.reset() // reset the router state
    injectRouterMock(router)
})
  
// Add properties to the wrapper
config.plugins.VueWrapper.install(VueRouterMock)


describe('Pruebas en el Login Component', () => {

    const store = createVuexStore({

        status: 'not-authenticated', // 'authenticated', 'not-authenticated', authenticating
        user: null, 
        idToken: null,
        refreshToken: null

    })

    store.dispatch = jest.fn() //función mock

    beforeEach( () => jest.clearAllMocks() )

    test('debe de hacer match con el snapshot ', () => {

        const wrapper = shallowMount( LoginView, {
            global: {
                plugins: [ store ]
            }
        })
        
        expect(wrapper.html()).toMatchSnapshot()

    })

    test('credenciales incorrectas disparan el error de SWAL ', async () => {

        store.dispatch.mockReturnValueOnce({ok: false, message: 'Error en credenciales'})

        const wrapper = shallowMount( LoginView, {
            global: {
                plugins: [ store ]
            }
        }) 

        await wrapper.find('form').trigger('submit')
        expect( store.dispatch ).toHaveBeenCalledWith('auth/signInUserAction',{email: '',password: ''})
        expect( Swal.fire ).toHaveBeenCalledWith("Error", "Error en credenciales", "error")
    })


    test('debe de redirigir a la ruta no-entry ', async() => {
        
        store.dispatch.mockReturnValueOnce({ok: true})

        const wrapper = shallowMount( LoginView, {
            global: {
                plugins: [ store ]
            }
        }) 

        const [ txtEmail, txtPassword ]  = wrapper.findAll('input')

        await txtEmail.setValue('email@email.com')
        await txtPassword.setValue('123456')

        await wrapper.find('form').trigger('submit')

        expect( store.dispatch ).toHaveBeenCalledWith("auth/signInUserAction", {email: "email@email.com", password: "123456"})
        expect( wrapper.router.push ).toHaveBeenCalledWith( {name: "no-entry"} )

    })


    
})

