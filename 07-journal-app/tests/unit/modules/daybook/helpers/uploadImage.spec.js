/* eslint-disable */
import axios from "axios"
import cloudinary from "cloudinary";

import uploadImage from "@/modules/daybook/helpers/uploadImage"


cloudinary.config({
    cloud_name: 'db6fiavaq',
    api_key: '951837836621631',
    api_secret: 'iVcIJ8OIkrOSoCHR8Nrgm5GIrgU'
})


describe('Pruebas en el uploadImage ', () => {
    
    test('debe de cargar un archivo y retornar la url', async() => {
        
        const { data } = await axios.get('https://res.cloudinary.com/db6fiavaq/image/upload/v1711550013/p5r119nikna6bfwfqifc.jpg',{
            responseType : 'arraybuffer'
        })


        const file = new File( [data] , 'foto.jpg')
        const url = await uploadImage( file )

        expect( typeof url ).toBe('string') 


        //* * borra la imagen subida para la prueba
        const segments = url.split('/')
        const imageId = segments[ segments.length - 1 ].replace('.jpg','')

        await cloudinary.v2.api.delete_resources( imageId)


    })


})