/* eslint-disable */

import { createLogger, createStore } from "vuex"
import journal from "@/modules/daybook/store/journal"
import { journalState } from "../../../../mock-data/test-journal-state"
import authApi from "@/api/authApi"

const createVuexStore = ( initialState ) => createStore({
    modules : {
        journal: {
            // desestructuramos el obj 'journal' 
            ...journal, 

            // sobreescribimos el 'state' desestructurado con el 'initialState' pasado por parámetro
            state: { ...initialState}
        }
    }
})

describe('Vuex - Pruebas en el Journal Module', () => {

    //antes de cada prueba nos autenticamos 
    beforeAll( async() => {

        const { data } = await authApi.post(':signInWithPassword', {
            email: 'test@test.com',
            password: '123456',
            returnSecureToken : true
        })

        localStorage.setItem('idToken', data.idToken)

    })
    

    // pruebas básicas 
    test('este es el estado inicial, debe de tener este state ', () => {
        
        const store = createVuexStore( journalState )

        //console.log(store.state)
        
        const { isLoading, entries} = store.state.journal

        expect(isLoading).toBeFalsy
        expect(entries).toEqual(journalState.entries)

    })

    // pruebas con mutations 
    test('mutations: setEntriesMutation ', () => {
      
        // emulamos el estado inicial de la app */
        const store = createVuexStore( { isLoading: true, entries: [] })

        // commit('nombre_modulo/nombre-mutacion', parametros)  */
        store.commit('journal/setEntriesMutation', journalState.entries)

        expect( store.state.journal.entries ).toEqual(journalState.entries)
        expect( store.state.journal.isLoading ).toBeFalsy()

    })

    test('mutations: updateEntryMutation ', () => {
        
        // create store con entries */
        const store = createVuexStore( journalState )

        

        // updatedEntry */
        const updatedEntry = {
            "id" : "-Ntz8MZnrYdo_QWEiGKj",
            "date": 1711530141919,
            "picture": "https://res.cloudinary.com/db6fiavaq/image/upload/v1711550013/p5r119nikna6bfwfqifc.jpg",
            "text": "Hola mundo desde pruebas"
        }

        // commit de la mutacion */
        store.commit('journal/updateEntryMutation', updatedEntry)

        // expects */
        const storeEntries = store.state.journal.entries

        // entries.length  debe ser igual a como estaba antes (no debe crecer) */ 
        expect( storeEntries.length ).toEqual(journalState.entries.length)

        // entries tiene que existir el updatedEntry (toEqual) */
        expect( storeEntries.find( (entrada) => entrada.id === updatedEntry.id ) ).toEqual(updatedEntry)
        
    })


    test('mutations: addEntryMutation ', () => {
        
        // create store con entries */
        const store = createVuexStore( journalState )

        // newEntry */
        const newEntry = {
            "id" : "500",
            "date": 1711530141919,
            "picture": "https://res.cloudinary.com/db6fiavaq/image/upload/v1711550013/p5r119nikna6bfwfqifc.jpg",
            "text": "Hola mundo desde pruebas"
        }

        // commit de la mutacion */
        store.commit('journal/addEntryMutation', newEntry)

        // expects */
        const storeEntries = store.state.journal.entries

        // entries.length  debe ser igual a como estaba antes (no debe crecer) */ 
        expect( storeEntries.length ).toEqual(journalState.entries.length+1)
    })


    test('mutations: deleteEntryMutation ', () => {
        
        // create store con entries */
        const store = createVuexStore( journalState )

        // clonamos el array original */
        const auxArr = [ ...store.state.journal.entries ]

        // le quitamos el último elemento al array clonado (y guardamos ese último elemento) */
        const deletedEntry = auxArr.pop()

        // commit de la mutacion */
        store.commit('journal/deleteEntryMutation', deletedEntry.id)

        // expects */

        // entries.length  debe ser igual a como estaba antes - 1 */ 
        expect( store.state.journal.entries.length ).toEqual(journalState.entries.length-1)

        // el array resultante debe ser igual al de origen menos el úiltimo elementi */
        expect( store.state.journal.entries ).toEqual(auxArr)
    })


    // pruebas con getters 
    test('getters:  getEntriesByTerm', () => {

        const store = createVuexStore( journalState )

        // obtenemos la función 'getEntriesByTerm' de los getters */
        const getEntriesByTermFunction = store.getters['journal/getEntriesByTerm']

        // ejecutamos la función (y nos retorna un array con todos los obbjetos) */
        const arrEntries = getEntriesByTermFunction('')

        expect( arrEntries.length ).toEqual(store.state.journal.entries.length)

        // desestructuramosel array de todas las entradas  */
        const [e0,e1,e2,e3,e4] = getEntriesByTermFunction('')

        expect( getEntriesByTermFunction('vacaciones')[0] ).toEqual( e0 )

    })

    test('getters:  getEntryById', () => {

        const store = createVuexStore( journalState )

        // obtenemos la función 'getEntriesByTerm' de los getters */
        const getEntriesByIdFunction = store.getters['journal/getEntryById']

        // ejecutamos la función (y nos retorna un obbjeto) */
        const entrada = getEntriesByIdFunction('-Nu-FqNwDI7b5CllT-iL')

        expect( entrada ).toEqual( store.state.journal.entries[3] )

    })


    // pruebas con actions 
    test('actions: loadEntriesAction', async () => {
        
        // emulamos el estado inicial de la app 
        const store = createVuexStore( { isLoading: true, entries: [] })

        // ojo q esto tb llama a firebase 
        await store.dispatch('journal/loadEntriesAction')

        // en firebase tenemos 5 entradas 
        expect( store.state.journal.entries.length ).toBeGreaterThan(0)

    })


    test('actions: updateEntryAction ', async () => {
  
        const store = createVuexStore( journalState )

        const updatedEntry = {
            id : "-NtyyKwyME2t773k8f2-",
            date : 1711527239214,
            picture : "https://res.cloudinary.com/db6fiavaq/image/upload/v1711549894/s69umo8tysaer71xzci0.png",
            text : "estoy deseando estar de vacaciones\n",
            otroCampo : true, 
            otroMas : {a : 1}
        }

        await store.dispatch('journal/updateEntryAction', updatedEntry)

        expect( store.state.journal.entries.length ).toBe( journalState.entries.length )

        expect( store.state.journal.entries.find(entrada => entrada.id === updatedEntry.id) ).toEqual( {

            id : "-NtyyKwyME2t773k8f2-",
            date : 1711527239214,
            picture : "https://res.cloudinary.com/db6fiavaq/image/upload/v1711549894/s69umo8tysaer71xzci0.png",
            text : "estoy deseando estar de vacaciones\n",

        } )

    })

    test('actions: createEntryAction y DeleteEntryAction ', async () => {

        // create store 
        const store = createVuexStore( journalState )

        //new Entry = {date: un nº mayor de cero, text: 'Nueva entrada desde las pruebas'}
        const newEntry = {
            date : 1711527233333,
            text : 'Nueva entrada desde las pruebas'
        }


        // dispatch de la accio de createEntry
        //obtener el ID de la nueva entrada (viene de Firebase)
        const nuevoId = await store.dispatch('journal/createEntryAction', newEntry)


        console.log('nuevoId -> ', nuevoId)

        //el id debe ser un string
        expect( (typeof nuevoId === "string") ).toBeTruthy()

        //la nueva entrada debe existir en el state.journal.entries....
        expect( store.state.journal.entries.find(entrada => entrada.id === nuevoId) ).not.toBeUndefined()

        //dispatch deleteEntry
        await store.dispatch('journal/deleteEntryAction', nuevoId)

        //la nueva entrada NO debe existir en el state.journal.entries....
        expect( store.state.journal.entries.find(entrada => entrada.id === nuevoId) ).toBeUndefined()

    })
})