/*  eslint-disable  */

import { createStore } from "vuex"
import { shallowMount } from "@vue/test-utils"
import journal from "@/modules/daybook/store/journal"
import { journalState } from "../../../mock-data/test-journal-state"
import EntryView from "@/modules/daybook/views/EntryView"

import Swal  from "sweetalert2"

const createVuexStore = ( initialState ) => createStore({
    modules : {
        journal: {
            // desestructuramos el obj 'journal' 
            ...journal, 

            // sobreescribimos el 'state' desestructurado con el 'initialState' pasado por parámetro
            state: { ...initialState}
        }
    }
})

//los parentesis rodeando las llaves indican que se está devolviendo un objeto js
jest.mock('sweetalert2', () => ({
    fire: jest.fn(),
    showLoading : jest.fn(),
    close : jest.fn()
}) )


describe('Pruebas en el EntryView', () => {


    const store = createVuexStore(journalState)

    //mockeamos  el dispatch del store
    store.dispatch = jest.fn()

    const mockRouter = {
        push: jest.fn()
    }

    let wrapper

    beforeEach( () => {

        jest.clearAllMocks()

        wrapper  = shallowMount( EntryView, {
            props: {
                id: '-NtyyKwyME2t773k8f2-'
            },
            global: {
                mocks:{
                    $router : mockRouter
                },
                plugins : [ store ]
            }
        })

    })

    
    test('debe de sacar al usuario porque el id no existe  ', () => {
      
        const wrapper  = shallowMount( EntryView, {
            props: {
                id : 'este ID no existe en el STORE'
            },
            global: {
                mocks:{
                    $router : mockRouter
                },
                plugins : [ store ], 
            
            }
        })

        expect( mockRouter.push ).toHaveBeenCalledWith({ name: 'no-entry' })
    })

    test('debe de mostrar la entrada correctamente', () => {
        
        expect(wrapper.html()).toMatchSnapshot()
        expect( mockRouter.push ).not.toHaveBeenCalledWith()

    });


    test('debe de borrar la entrada y salir ', (done) => {

        //fingimos un isConfirned = true (llamamos a la funcion 'mockReturnValueOnce' de jest.fn())
        Swal.fire.mockReturnValueOnce( Promise.resolve({ isConfirmed: true }) )

        wrapper.find('.btn-danger').trigger('click')

        expect( Swal.fire ).toHaveBeenCalledWith({
            title : '¿está seguro?',
            text: 'Una vez borrado, no se puede recuperar',
            showDenyButton : true,
            confirmButtonText : 'Sí, estoy seguro'
        })

        //esperamos una milésima de segundo y llamamos al expect
        setTimeout( () => {

            expect( store.dispatch ).toHaveBeenCalledWith('journal/deleteEntryAction', '-NtyyKwyME2t773k8f2-')
            expect( mockRouter.push  ).toHaveBeenCalled()
            done()

        }, 1)


        

    });


})

