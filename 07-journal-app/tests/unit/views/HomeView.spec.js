
/* eslint-disable */

import { shallowMount } from "@vue/test-utils"
import HomeView from "@/views/HomeView"


describe('Pruebas en el HomeView', () => {

    

    test('Debe hacer match ok con el snapshot ', () => {

        const wrapper = shallowMount( HomeView )

        expect(wrapper.html()).toMatchSnapshot()

    })

    test('hacer click en un botón debe de redireccionar a no-enrty ', () => {
        
        const mockRouter = {

            push: jest.fn() //** propio de jest */

        }

        const wrapper = shallowMount( HomeView, {
            global: {
                mocks: {
                    $router : mockRouter
                }
            }
        })

        wrapper.find('button').trigger('click')

        expect( mockRouter.push ).toHaveBeenCalled()
        expect( mockRouter.push ).toHaveBeenCalledWith({ name: 'no-entry'})
    
    })

})