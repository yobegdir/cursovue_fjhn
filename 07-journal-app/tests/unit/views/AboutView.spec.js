/* eslint-disaable */

import { shallowMount } from "@vue/test-utils"
import AboutView from "@/views/AboutView"


describe('Pruebas en el AboutView', () => {

    let wrapper

    beforeEach(() => {

        wrapper = shallowMount( AboutView )

    })

    test('Debe hacer match ok con el snapshot ', () => {
      
        expect(wrapper.html()).toMatchSnapshot()

    })

})



