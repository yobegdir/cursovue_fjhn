/* eslint-disable */

// export const myAction = async ({ commit }) => {

// }

import journalApi from "@/api/journalApi"

export const loadEntriesAction = async ({ commit }) => {

    // devuelve una promesa , esa promesa la asignamos a una variable que a su vez desestructuramos y obtemos el valor 'data' */
    const {data} = await journalApi.get('/entries.json')

    if (!data){

        commit('setEntriesMutation', [])
        return 
    
    } 


    const entries = []


    // llenamos el array 'entries'
    for ( let id of Object.keys( data ) ){
        entries.push({
            id,
            // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
            ...data[id]
        })
    }

    //  hacemos un commit de la mutacion 'setEntries',  a la cual le pasamos como parámetro el array 'entries'
    commit('setEntriesMutation', entries)

}


export const updateEntryAction = async ({ commit }, entry) => { //entry debe ser un parametro

    //extraer lo que necesitamos (quitar el id)
    const { date, picture, text } = entry

    const dataToSave = { date, picture, text }

    //peticion http journalApi.put(PATHy q acabe con un .json, dataToSave (la info q queremos guardar)
    const resp = await journalApi.put(`/entries/${ entry.id }.json`, dataToSave )

    dataToSave.id = entry.id

    // commit de una mutation  -> updateEntry (rompiendo la referencia -> {...<nom_variable>})
    commit('updateEntryMutation', { ...dataToSave })

}


export const createEntryAction = async ({ commit }, entry ) => {

    // dataToSave
    const { date, picture, text } = entry
    const dataToSave = { date, picture, text }


    const { data } = await journalApi.post(`/entries.json`, dataToSave )
    
    dataToSave.id = data.name

    commit('addEntryMutation', dataToSave)

    return data.name
}


export const deleteEntryAction = async ({ commit }, id) => {

    //await journalApi.delete(path)
    await journalApi.delete(`/entries/${ id }.json`)
    
    //commit deleteEntryMutation
    commit('deleteEntryMutation', id)

    return id
}