/* eslint-disable */

// export const myGetter =  ( state ) => {
//  state.loQueSea    
// }


//  lambda que llama a una funcion ( term = '' ) => {...}
export const getEntriesByTerm =  ( state ) => ( term = '' ) => {

    //console.log('term: ', term);


    if (term.length === 0) return state.entries

    return state.entries.filter( entry => entry.text.toLowerCase().includes( term.toLocaleLowerCase() ))
}


//  lambda que llama a una funcion ( id = '' ) => {...}
export const getEntryById = ( state ) => ( id = '' ) => {

    //console.log(state.entries);

    
    const entry = state.entries.find( entry => entry.id === id)

    if ( !entry ) return

    return { ...entry } // (hacemos esto para no devolver una referencia al state (si modificasemos ese obj retornado desde fuera de 'getEntryById' tb modificariamos el state 
                        // y el state solo se puede modificar desde una mutation)
}