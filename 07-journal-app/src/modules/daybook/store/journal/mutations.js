/* eslint-disable */

// export const myMutations = ( state ) => {

// }


export const setEntriesMutation = ( state, entries) => {

    //** expando las entradas q hubiera en el state y tb las nuevas que llegan
    state.entries = [ ...state.entries, ...entries ]
    state.isLoading = false

}


export const updateEntryMutation = ( state, entry ) => { // debe recibir el entry actualizado

    //creamos un array SOLO de ids (con map) y en ese array aplanado buscamos el id del entry
    const idx = state.entries.map(e => e.id).indexOf(entry.id)

    //al tocar el state, se actualiza en todos lados (incluido el firebase)
    state.entries[idx] = entry
}

export const addEntryMutation = ( state, entry ) => {

    state.entries = [ entry, ...state.entries ]
    
}

export const deleteEntryMutation = ( state, id ) => {

    //remover del state.entries el q coincida con el id
    state.entries = state.entries.filter(entry => entry.id !== id)

}


export const clearEntriesMutation = ( state ) => {

    state.entries = []

}