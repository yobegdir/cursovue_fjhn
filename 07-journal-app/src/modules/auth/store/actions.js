/* eslint-disable */

import authApi from "@/api/authApi"


export const createUserAction = async ({ commit }, user) => {

    const { name, email, password } = user

    // console.log('name', name)
    // console.log('email', email)
    // console.log('password', password)

    try {
        
        const { data } = await authApi.post(':signUp', { email, password, returnSecureToken : true })
        const { idToken, refreshToken } = data


        const resp = await authApi.post(':update', { displayName: name, idToken })

        
        delete user.password
        commit('loginUserMutation', { user, idToken, refreshToken })


        return { ok: true }

    } catch (error) {
        return { ok:false, message: error.response.data.error.message }
    }


}

export const signInUserAction = async ({ commit }, user) => {

    const { email, password } = user

    
    // console.log('email', password)
    // console.log('password', password)

    try {
        
        const { data } = await authApi.post(':signInWithPassword', { email, password, returnSecureToken : true })
        const { displayName, idToken, refreshToken } = data

        //console.log(data)

        user.name = displayName

        commit('loginUserMutation', { user, idToken, refreshToken })


        return { ok: true }

    } catch (error) {
        return { ok:false, message: error.response.data.error.message }
    }
}


export const checkAuthenticationAction = async ({ commit }) => {

    const idToken = localStorage.getItem('idToken')
    const refreshToken = localStorage.getItem('refreshToken')

    if ( !idToken ){
        commit('logoutMutation')
        return { ok: false, message: 'No hay token en la petición' }
    }

    try {
        
        const { data } = await authApi.post(':lookup',{ idToken })
        
        const { displayName, email } = data.users[0]

        const user = {
            name : displayName, 
            email
        }

        commit('loginUserMutation', { user, idToken, refreshToken })

        return { ok : true }

    } catch (error) {
        commit('logoutMutation')
        return { ok: false, message: error.response.data.error.message }
    }

}