/* eslint-disable */

import { computed } from 'vue'
import { useStore } from "vuex"

const useAuth = () => {


    const store = useStore()


    const createUserAction = async( user ) => {

        //llamamos al ac5tion correspondiente
        const resp = await store.dispatch('auth/createUserAction', user)
        return resp
    }

    const loginUserAction = async( user ) => {
        const resp = await store.dispatch('auth/signInUserAction', user)
        return resp
    }

    const checkStatusAction = async () =>  {
        const resp = await store.dispatch('auth/checkAuthenticationAction')
        return resp
    }

    const logout = () =>{
        store.commit('auth/logoutMutation')

        //limpiar las entradas del store del journal
        store.commit('journal/clearEntriesMutation')
    }


    return {
        createUserAction,
        loginUserAction,
        checkStatusAction,
        logout,

        authStatus : computed( () => store.getters['auth/currentState']  ),
        username : computed( () => store.getters['auth/username']  )

    }
}

export default useAuth