/* eslint-disable */

//el store padre, no el del módulo
import store from "@/store"

const isAuthenticatedGuard = async(to, from, next) => {

    const { ok } = await store.dispatch('auth/checkAuthenticationAction')


    if( ok ) next() //le dejamos pasar
    else next({ name: 'login' }) //vamos a la ruta del login


}

export default isAuthenticatedGuard
