/* eslint-disable */

import axios from "axios";

const journalApi = axios.create({
    baseURL : 'https://vue-demos-7ecf1-default-rtdb.europe-west1.firebasedatabase.app'
})

//man in the middle (x cada request)
journalApi.interceptors.request.use( (config) => {

    config.params = {
        auth : localStorage.getItem('idToken')
    } 

    //https://stackoverflow.com/questions/72589579/what-is-canceltoken-by-axios-and-how-i-fix-it
    return config

} )



//console.log( process.env.NODE_ENV ) //'test' durante testing (node parado)
                                      //'development' con el node levantado en local

export default journalApi