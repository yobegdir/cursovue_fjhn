/* eslint-disable */
import { createStore } from "vuex"

/*
    apuntamos a la carpeta de 'journal' xq el tolai del profesor nos dió una estructura modularizada en el store de dayvbook con 
    dos sub módulos: daybook (q no lo usa) y journal . Y cada uno de ellos debería tener su 'index.js'. En este caso nosotros
    apuntamos al index.js de 'modules/daybook/store/journal' y al de 'modules/auth/store'
*/
import journal from "../modules/daybook/store/journal" 
import auth from "../modules/auth/store"


const store = createStore({

    modules : {
        auth,
        journal
    }

})


export default store
