/* eslint-disable */

// ** por defecto se recibe el 'state'
export const increment = ( state ) => {
    state.count++
    state.lastMutation = 'increment'
}

export const incrementBy = ( state, valor ) => {
    state.count += valor
    state.lastMutation = 'incrementBy ' + valor
    state.lastRandomInt = valor
}

export const setLoading = (state, habilitado) => {
    state.isLoading = habilitado
    state.lastMutation = 'setLoading ' + habilitado
}