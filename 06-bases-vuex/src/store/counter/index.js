/* eslint-disable */

//**el estado de la app
import state from "./state"

//** las mutaciones modifican el estado del 'state' (se invocan las mutaciones con commits)
import * as mutations from './mutations'

//** las acciones invocan (hacen commits) de las mutaciones
import * as actions from './actions'

//** funcionan como las propiedades computadas, para obtener info procesada o cierta lógica del 'state'
import * as getters from './getters'

const counterStore  = {
    
    //** esto en el 'index.js'  de cada módulo
    namespaced: true,

    //** hacemos reactivo el 'state' metiéndolo en un lambda
    state, 

    //** son comiteadas
    mutations,

    //** son despachadas (commitean  mutaciones)
    actions,
    
    //** son muy parecidos a las computed*/
    getters
    
}


export default counterStore