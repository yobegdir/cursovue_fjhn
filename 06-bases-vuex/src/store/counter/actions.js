/* eslint-disable */

import getRandomInt from "../../helpers/getRandomInt";


// ** por defecto se recibe el 'context'
export const incrementRandomInt = async( {commit} ) => {

    commit('setLoading', true)

    const randomInt = await getRandomInt()

    //** comiteamos a la mutacion 'incrementBy'
    commit('incrementBy', randomInt)

    commit('setLoading', false)

}