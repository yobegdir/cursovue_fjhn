/* eslint-disable */

import { ref } from "vue"
import axios from "axios"

const usePokemon = ( pokemonId = 1 ) => {

    const pokemon = ref()
    const isLoading = ref(false)
    const errorMessage = ref(null)


    const searchPokemon = async (id) => {


        if( !id ) return 

        //console.log('yeah')

        //console.log('pokemonId', pokemonId)


        isLoading.value = true
        pokemon.value = null

        try {
            
            //https://pokeapi.co/api/v2/

            const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${ id }`)
            pokemon.value = data
            errorMessage.value = null


            //console.log(data)


        } catch (error) {

            //console.log('error', error)


            errorMessage.value = "No se pudo caregar ese pokemon"


        }

        isLoading.value = false
    }


    searchPokemon( pokemonId )


    return{
        pokemon,
        isLoading,
        errorMessage,
        searchPokemon
    }

}


export default usePokemon