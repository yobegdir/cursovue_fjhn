/* eslint-disable */

import { computed, ref } from 'vue'
import { useStore } from 'vuex'


const useTodos = () => {

    const store = useStore()

    const currentTab = ref('all')     

    return{
        currentTab,

        //si solo lo va a usar el template, mejor definir los getters aqui.
        //si los getters se usan en el cuerpo del setup(), entonces  definirlos al ppio del setup()
        pending : computed( () => store.getters['pendingTodos'] ),
        all : computed( () => store.getters['allTodos'] ),
        completed : computed( () => store.getters['completedTodos'] ),

        getTodosByTab : computed( () => store.getters['getTodosByTab'](currentTab.value) ),
        
        //methods
        toggleTodo : ( id ) => store.commit('toggleTodo', id),
        createTodo : ( text ) => store.commit('createTodo', text)
        
    }
}


export default useTodos
