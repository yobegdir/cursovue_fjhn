/* eslint-disable*/

import { ref } from 'vue'

const useCounter = ( initValue = 5 ) => {

    //variable reactiva (valor inicial), la envuelve el 'ref'
    const counter = ref( initValue )

    //aqui ponemos lo que queramos que sea visible en el template o para otros componentes
    return{
        counter,
        
        //funciones en directamente en el return
        increase: () => counter.value++,
        decrease: () => counter.value--
    }

}

export default useCounter

