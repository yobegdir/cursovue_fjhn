import { createStore } from 'vuex'
import { v4 as uuidv4 } from 'uuid'

export default createStore({
  //el estado global (rootstate)
  state: {
    todos:[
      { id: '1', text: 'Recolectar las piedras del infinito', completed: false },
      { id: '2', text: 'Piedra del alma', completed: true },
      { id: '3', text: 'Piedra de poder', completed: true },
      { id: '4', text: 'Piedra de realidad', completed: false },
      { id: '5', text: 'Conseguir nuevos secuaces competentes', completed: false },
    ]
  },
  mutations: {
    toggleTodo( state , id){
      const todoIdx = state.todos.findIndex(todo => todo.id === id)
      console.log(todoIdx)
      state.todos[todoIdx].completed = !state.todos[todoIdx].completed
    }, 
    createTodo( state, text = ''){

      if ( text.length <= 1 ) return

      //insertamos la nueva tarea
      state.todos.push({
        id : uuidv4(), 
        completed : false, 
        text
      })
    }
  },
  actions: {
  },
  getters: {
    //state hace referencia al estado del módulo y el rootState al global (en este caso son el mismo)
    pendingTodos( state, getters, rootState ){
      return state.todos.filter( todos => !todos.completed )
    },

    //state hace referencia al estado del módulo y el rootState al global (en este caso son el mismo)
    allTodos: (state, getters, rootState) => {
      return [...state.todos] //desestructuramos y evitamos modificar el state por referencia
    },

    //state hace referencia al estado del módulo y el rootState al global (en este caso son el mismo)
    completedTodos: (state, getters, rootState) => {
      return state.todos.filter( todos => todos.completed )
    },

    //el primer argumento es obligatorio, pero no lo usamos  por eso ponemos el _
    getTodosByTab: ( _ , getters) => {
      //funcion que llama a otra función en su cuerpo
      const aux = (tab) => {
      
        //devolvemos el getter que corresponda
        switch (tab) {
          case 'all': return getters.allTodos
          case 'pending': return getters.pendingTodos
          case 'completed': return getters.completedTodos
        }
      }

      //y devolvemos la función
      return aux
    }

  },
  modules: {
  }
  
})
